# OpenTelemetry GitLab Research

## Research Scope

"Research OpenTelemetry with Tracing to help achieve CI/CD Observability in GitLab."

- [CI/CD Observability with OpenTelemetry feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/338943)
- [FY23 Observability strategy, Michael, Dev Evangelism](https://gitlab.com/groups/gitlab-com/marketing/-/epics/2593)

Owner: [@dnsmichi](https://gitlab.com/dnsmichi)

## Purpose

Collect insights and ideas, and document the contribution experience. This research project consists of multiple paths:

1. [GitLab Runner (Go)](research-gitlab-runner.md)
2. [GitLab Server (Ruby)](research-gitlab-server.md)
3. Anything else: Customization, Scaling, Security, Deployments, Agent for Kubernetes, etc. 

## Changelog

Follow the updates in the [Changelog](changelog.md).

## Resources

- [Proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/338943)
- [OpenTelemetry](https://opentelemetry.io/)
  - [Collector: Getting Started](https://opentelemetry.io/docs/collector/getting-started/)
  - [Collector: Configuration](https://opentelemetry.io/docs/collector/configuration/)
- [Runner development](https://docs.gitlab.com/runner/development/)
- [GitLab Go Development Guide](https://docs.gitlab.com/ee/development/go_guide/)
- Implementation and learnings to follow:
- https://gitlab.com/everyonecancontribute/observability/gitlab-runner-opentelemetry/-/merge_requests/1 

Learning guides

- https://opentelemetry.lightstep.com/core-concepts/context-propagation/
- https://docs.honeycomb.io/getting-data-in/opentelemetry/go/#printing-to-the-console
- https://seankhliao.com/blog/12021-02-12-otel-go-v0.16.0-tracing/

### Development Environment

- [dev-env-otel.md](dev-env-otel.md) provides detailed instructions for different deployment methods.
- [dev-env-gitlab-runner.md](dev-env-gitlab-runner.md) provides GitLab Runner instructions and builds.




