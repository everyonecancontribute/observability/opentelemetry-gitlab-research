# Changelog

Updates, milestones, recordings, etc. 

## 2022-05-05: First Go instrumentation with GitLab Runner

[Issue update](https://gitlab.com/gitlab-org/gitlab/-/issues/338943#note_936910290)

[![2022-05-05 Research Update](http://img.youtube.com/vi/h1rx8lHNXQs/0.jpg)](https://www.youtube.com/watch?v=h1rx8lHNXQs)


### Research Summary

- Architecture Analysis
- GitLab Runner Development Environment
- First steps with OpenTelemetry Go SDK
- Instrumentation with Go code of Runner process, printed to stdout
- OpenTelemetry Collector standalone, logging to stdout
- OpenTelemetry Collector with Jaeger Tracing in docker-compose
- First traces sent from executed runner jobs to OpenTelemetry, queried in Jaeger Query UI 
- Notes, config, documentation in https://gitlab.com/everyonecancontribute/observability/opentelemetry-gitlab-runner-playground 

### Next steps 

For [cdCon talk](https://cdcon2022.sched.com/event/10UYj):

- From Runner to Server - dive into pipeline execution Ruby code & OpenTelemetry Ruby SDK
- Tracing setup with Opstrace and OpenTelemetry receiver. My own tries with OpenTelemetry/Jaeger/Clickhouse Operator in Kubernetes failed (but are documented in https://gitlab.com/everyonecancontribute/observability/opentelemetry-gitlab-runner-playground/-/blob/main/dev-env-otel.md#kubernetes)

### Future steps

- Enrich Runner code with more traces and context metadata, create a draft MR
- Same for server code
- Combine Server/Runner traces
- Research on Customization, Security, Scaling, LabKit & SRE integration 
- MRs for code, and docs both GitLab and OpenTelemetry upstream 

